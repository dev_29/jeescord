<?php

/* This file is part of Jeedom.
 *
 * Jeedom is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jeedom is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Jeedom. If not, see <http://www.gnu.org/licenses/>.
 */

/* * ***************************Includes********************************* */
require_once __DIR__.'/../../../../core/php/core.inc.php';
class jeescord extends eqLogic {
    public function  SendMSG($_options = array()){
      $URL = $this->getConfiguration("WHURL");
            $IconURL = $this->getConfiguration("IconURL");
            $UserName = $this->getConfiguration("UserName");
        if ($this->getConfiguration("sendmode") == "CONTENT"){
	    $postdata = array(
		"username" => $UserName,
		"icon_url" => $IconURL,
		"content" => $_options["title"]."\n".$_options["message"]
	    );
	}else{
            $postdata =  array(
                    "username" => $UserName,
                    "icon_url" => $IconURL,
                    "embeds" => array(array(
                        "description" => $_options["message"],
                        "title" => $_options["title"],
                   ))
            );
	}
        $this->PostRQ($URL,$postdata);
    }
    public function PostRQ($url,$data){
//create a new cURL resource
$ch = curl_init($url);
$payload = json_encode($data);

//attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute the POST request
$result = curl_exec($ch);

//close cURL resource
curl_close($ch);

    return $result;
  }
    /*     * *************************Attributs****************************** */



    /*     * ***********************Methode static*************************** */

    /*
     * Fonction exécutée automatiquement toutes les minutes par Jeedom
      public static function cron() {

      }
     */


    /*
     * Fonction exécutée automatiquement toutes les heures par Jeedom
      public static function cronHourly() {

      }
     */

    /*
     * Fonction exécutée automatiquement tous les jours par Jeedom
      public static function cronDaily() {

      }
     */



    /*     * *********************Méthodes d'instance************************* */

    public function preInsert() {

    }

    public function postInsert() {

    }

    public function preSave() {

    }

    public function postSave() {

        $info = $this->getCmd(null, 'messagesend');
        if (!is_object($info)) {
            $info = new jeescordCmd();
            $info->setName(__('Envoyer', __FILE__));
        }
        $info->setLogicalId('messagesend');
        $info->setEqLogic_id($this->getId());
        $info->setType('action');
        $info->setSubType('message');
        $info->save();
    }


    public function preUpdate() {

    }

    public function postUpdate() {

    }

    public function preRemove() {

    }

    public function postRemove() {

    }

    /*
     * Non obligatoire mais permet de modifier l'affichage du widget si vous en avez besoin
      public function toHtml($_version = 'dashboard') {

      }
     */

    /*
     * Non obligatoire mais ca permet de déclencher une action après modification de variable de configuration
    public static function postConfig_<Variable>() {
    }
     */

    /*
     * Non obligatoire mais ca permet de déclencher une action avant modification de variable de configuration
    public static function preConfig_<Variable>() {
    }
     */

    /*     * **********************Getteur Setteur*************************** */
}

class jeescordCmd extends cmd {

    /*     * *************************Attributs****************************** */


    /*     * ***********************Methode static*************************** */


    /*     * *********************Methode d'instance************************* */

    /*
     * Non obligatoire permet de demander de ne pas supprimer les commandes même si elles ne sont pas dans la nouvelle configuration de l'équipement envoyé en JS
      public function dontRemoveCmd() {
      return true;
      }
     */

    public function execute($_options = array()) {
        $eqLogic = $this->getEqLogic();
        if ($this->getLogicalId() == 'messagesend') {
            $rs = $eqLogic->SendMSG($_options);
	 }
      }

        /*     * **********************Getteur Setteur*************************** */
    }

